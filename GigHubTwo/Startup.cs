﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GigHubTwo.Startup))]
namespace GigHubTwo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
