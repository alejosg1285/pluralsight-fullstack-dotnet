﻿using GigHubTwo.Core;
using GigHubTwo.Dtos;
using GigHubTwo.Models;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace GigHubTwo.Controllers.api
{
    [Authorize]
    public class FollowingsController : ApiController
    {
        private IUnitOfWork _unitOfWork;

        public FollowingsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        public IHttpActionResult Follow(FollowingDto dto)
        {
            var userId = User.Identity.GetUserId();

            if (_unitOfWork.Followees.CheckFollowing(dto.FolloweeId, userId)) return BadRequest("Following already exists");

            var following = new Following
            {
                FollowerId = userId,
                FolloweeId = dto.FolloweeId
            };

            _unitOfWork.Followees.Add(following);
            _unitOfWork.Complete();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult UnFollow(string id)
        {
            var userId = User.Identity.GetUserId();

            var following = _unitOfWork.Followees.GetFollowing(userId, id);

            if (following == null) return NotFound();

            _unitOfWork.Followees.Remove(following);
            _unitOfWork.Complete();


            return Ok();
        }
    }
}
