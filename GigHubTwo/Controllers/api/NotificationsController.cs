﻿using GigHubTwo.Core;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace GigHubTwo.Controllers.api
{
    [Authorize]
    public class NotificationsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public NotificationsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //public IEnumerable<NotificationDto> GetNewNotifications()
        //{
        //    var userId = User.Identity.GetUserId();

        //    var notifications = _unitOfWork.Notifications.GetNotificationsByUser(userId);

        //    return notifications.Select(Mapper.Map<Notification, NotificationDto>);
        //}

        [HttpPost]
        public IHttpActionResult MarkAsRead()
        {
            var userId = User.Identity.GetUserId();
            var notifications = _unitOfWork.Notifications.GetUserNotificationsByUser(userId);

            notifications.ForEach(n => n.Read());

            _unitOfWork.Complete();

            return Ok();
        }
    }
}
