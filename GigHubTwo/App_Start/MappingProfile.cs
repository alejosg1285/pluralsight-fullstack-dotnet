﻿using AutoMapper;
using GigHubTwo.Dtos;
using GigHubTwo.Models;

namespace GigHubTwo.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, UserDto>();
            CreateMap<Gig, GigDto>();
            CreateMap<Notification, NotificationDto>();
        }
    }
}