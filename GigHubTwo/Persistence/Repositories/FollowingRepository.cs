﻿using GigHubTwo.Models;
using System.Linq;

namespace GigHubTwo.Repositories
{
    public class FollowingRepository : IFollowingRepository
    {
        private readonly ApplicationDbContext _context;

        public FollowingRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(Following following)
        {
            _context.Followings.Add(following);
        }

        public bool CheckFollowing(string followeeId, string userId)
        {
            return _context.Followings.Any(f => f.FolloweeId == followeeId && f.FollowerId == userId);
        }

        public Following GetFollowing(string userId, string followeeId)
        {
            return _context.Followings
                                .SingleOrDefault(f => f.FolloweeId == followeeId && f.FollowerId == userId);
        }

        public void Remove(Following following)
        {
            _context.Followings.Remove(following);
        }
    }
}