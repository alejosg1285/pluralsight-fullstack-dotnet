﻿using GigHubTwo.Core;
using GigHubTwo.Core.Repositories;
using GigHubTwo.Models;
using GigHubTwo.Persistence.Repositories;
using GigHubTwo.Repositories;

namespace GigHubTwo.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public IAttendanceRepository Attendances { get; private set; }
        public IFollowingRepository Followees { get; private set; }
        public IGenreRepository Genres { get; private set; }
        public IGigRepository Gigs { get; private set; }
        public INotificationRepository Notifications { get; private set; }

        //public FollowingRepository 

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Attendances = new AttendanceRepository(_context);
            Genres = new GenreRepository(_context);
            Gigs = new GigRepository(_context);
            Followees = new FollowingRepository(_context);
            Notifications = new NotificationRepository(_context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}