﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GigHubTwo.Dtos
{
    public class FollowingDto
    {
        public string FolloweeId { get; set; }
    }
}