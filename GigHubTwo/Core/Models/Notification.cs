﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GigHubTwo.Models
{
    public class Notification
    {
        public int Id { get; private set; }
        public DateTime DateTime { get; private set; }
        public NotificationType Type { get; private set; }
        public DateTime? OriginalDateTime { get; private set; }
        public string OriginalVenue { get; private set; }

        [Required]
        public Gig Gig { get; private set; }

        protected Notification() { }

        private Notification(Gig gig, NotificationType notificationType)
        {
            if (gig == null)
                throw new ArgumentNullException("gig");

            if (notificationType == null)
                throw new ArgumentNullException("notificationType");

            Gig = gig;
            Type = notificationType;
            DateTime = DateTime.Now;
        }

        // Factory methods
        public static Notification GigCreated(Gig gig)
        {
            return new Notification(gig, NotificationType.GigCreated);
        }

        public static Notification GigUpdated(Gig newGig, DateTime originalDateTime, string originalVenue)
        {
            var notification = new Notification(newGig, NotificationType.GigUpdated);
            notification.OriginalDateTime = originalDateTime;
            notification.OriginalVenue = originalVenue;

            return notification;
        }

        public static Notification GigCanceled(Gig gig)
        {
            return new Notification(gig, NotificationType.GigCanceled);
        }
    }
}