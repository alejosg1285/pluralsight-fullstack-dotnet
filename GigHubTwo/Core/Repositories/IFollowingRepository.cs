﻿using GigHubTwo.Models;

namespace GigHubTwo.Repositories
{
    public interface IFollowingRepository
    {
        Following GetFollowing(string userId, string followeeId);
        bool CheckFollowing(string followeeId, string userId);
        void Add(Following following);
        void Remove(Following following);
    }
}
