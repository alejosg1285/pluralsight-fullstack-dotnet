﻿using GigHubTwo.Models;
using System.Collections.Generic;

namespace GigHubTwo.Repositories
{
    public interface IGigRepository
    {
        Gig GetGig(int gigId);
        Gig GetGigWithAttendee(int gigId);
        IEnumerable<Gig> GetGigsUserAttending(string userId);
        void Add(Gig gig);
        List<Gig> GetUpcomingGigsByArtist(string userId);
        List<Gig> GetUpcomingGigsByFilter(string query);
        Gig GetGigByArtist(int gigId, string artistId);
    }
}
