﻿using GigHubTwo.Models;
using System.Collections.Generic;

namespace GigHubTwo.Core.Repositories
{
    public interface INotificationRepository
    {
        List<Notification> GetNotificationsByUser(string userId);
        List<UserNotification> GetUserNotificationsByUser(string userId);
    }
}