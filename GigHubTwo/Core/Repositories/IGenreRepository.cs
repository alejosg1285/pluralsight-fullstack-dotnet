﻿using GigHubTwo.Models;
using System.Collections.Generic;

namespace GigHubTwo.Repositories
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> GetGenres();
    }
}
