﻿using GigHubTwo.Core.Repositories;
using GigHubTwo.Repositories;

namespace GigHubTwo.Core
{
    public interface IUnitOfWork
    {
        public IAttendanceRepository Attendances { get; }
        public IFollowingRepository Followees { get; }
        public IGenreRepository Genres { get; }
        public IGigRepository Gigs { get; }
        public INotificationRepository Notifications { get; }
        void Complete();
    }
}
